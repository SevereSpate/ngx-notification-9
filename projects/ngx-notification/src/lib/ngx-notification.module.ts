import { CommonModule } from '@angular/common';
import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { NgxNotification } from './components/notification/notification.component';
import { NgxNotificationsComponent } from './components/ngx-notifications/ngx-notifications.component';
import { DEFAULT_OPTIONS } from './consts/default-options.const';
import { Options } from './interfaces/options.type';
import { NgxNotificationsService } from './services/notifications.service';
export const OPTIONS = new InjectionToken<Options>('options');
export function optionsFactory(options) {
  return {
    ...DEFAULT_OPTIONS,
    ...options
  };
}


@NgModule({
  declarations: [
    NgxNotificationsComponent,
    NgxNotification
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NgxNotificationsComponent
  ]
})
export class NgxNotificationModule {
  static forRoot(options: Options = {}): ModuleWithProviders<NgxNotificationModule> {
    return {
      ngModule: NgxNotificationModule,
      providers: [
        NgxNotificationsService,
        {
          provide: OPTIONS,
          useValue: options
        },
        {
          provide: 'options',
          useFactory: optionsFactory,
          deps: [OPTIONS]
        }
      ]
    };
  }
 }
